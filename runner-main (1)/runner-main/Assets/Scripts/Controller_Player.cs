﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private int jumpsRemaining = 2; // Número de saltos restantes

    // Power-up de invencibilidad
    private bool invincible = false;
    private float invincibilityDuration = 10f; // Duración de la invencibilidad
    private bool invincibilityActivated = false;
    public ParallaxSystem parallaxSystem;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
        CheckInvincibility();
    }

    private void GetInput()
    {
        Jump();
        Duck();
        ActivateInvincibility(); // Agregamos la función para activar la invencibilidad
    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                jumpsRemaining = 1; // Restablecer los saltos restantes cuando el jugador salta desde el suelo
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W) && jumpsRemaining > 0)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                jumpsRemaining--; // Reducir el número de saltos restantes
            }
        }
    }



    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void ActivateInvincibility()
    {
        if (!invincibilityActivated && Input.GetKeyDown(KeyCode.E))
        {
            invincibilityActivated = true;
            invincible = true;
            Invoke("DeactivateInvincibility", invincibilityDuration);
        }
    }

    private void DeactivateInvincibility()
    {
        invincible = false;
    }

   private void CheckInvincibility()
{
    // Si el jugador es invencible, cambia el color del material del GameObject
    if (invincible)
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.color = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time, 1));
    }
}

    public void OnCollisionEnter(Collision collision)
    {
        if (!invincible && collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
            parallaxSystem.StopParallax();
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
