﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies; // Prefabs de los enemigos
    public GameObject instantiatePos; // Posición de instanciación
    public float respawningTimer; // Tiempo entre cada instanciación
    private float time = 0;

    void Start()
    {
        // Configurar la velocidad del enemigo original
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        // Instanciar enemigos y cambiar velocidad
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        // Cambiar la velocidad del enemigo original suavemente
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        // Decrementar el temporizador
        respawningTimer -= Time.deltaTime;

        // Si el temporizador llega a cero
        if (respawningTimer <= 0)
        {
            // Instanciar un enemigo aleatorio
            GameObject enemyPrefab = enemies[UnityEngine.Random.Range(0, enemies.Count)];
            Instantiate(enemyPrefab, instantiatePos.transform.position, Quaternion.identity);

            // Reiniciar el temporizador con un nuevo valor aleatorio
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
}
