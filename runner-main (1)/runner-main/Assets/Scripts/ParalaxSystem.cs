using UnityEngine;

public class ParallaxSystem : MonoBehaviour
{


    // Método para detener el parallax
    public void StopParallax()
    {
        // Iterar sobre todos los objetos de parallax y detener su movimiento
        foreach (Transform child in transform)
        {
            child.GetComponent<Parallax>().enabled = false;
        }
    }


}
