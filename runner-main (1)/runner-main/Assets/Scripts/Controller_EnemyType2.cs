using UnityEngine;

public class Controller_EnemyType2 : MonoBehaviour
{
    public float movementSpeed = 5f; // Velocidad de movimiento

    void Update()
    {
        // Generar un vector de movimiento aleatorio
        Vector3 randomMovement = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f).normalized;
        // Mover el enemigo en la dirección aleatoria
        transform.Translate(randomMovement * movementSpeed * Time.deltaTime);
    }
}
