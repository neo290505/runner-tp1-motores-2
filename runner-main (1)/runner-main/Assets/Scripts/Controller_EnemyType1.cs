using UnityEngine;

public class Controller_EnemyType1 : MonoBehaviour
{
    public float movementSpeed = 1f; // Velocidad de movimiento
    public float amplitude = 1f; // Amplitud del movimiento
    private Vector3 initialPosition; // Posición inicial

    void Start()
    {
        initialPosition = transform.position; // Almacenar la posición inicial
    }

    void Update()
    {
        // Mover el enemigo hacia arriba y hacia abajo
        float verticalMovement = Mathf.Sin(Time.time * movementSpeed) * amplitude;
        transform.position = initialPosition + new Vector3(0f, verticalMovement, 0f);
    }
}
